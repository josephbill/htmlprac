<!DOCTYPE html>
<html>
<head>
	<title>Movie Store</title>
	<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.css">

	<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap-grid.css">
	<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap-grid.min.css">
	<link rel="stylesheet" type="text/css" href="../css/style.css">
</head>
<body>
  <?php
require 'uploadMovie.php';
   ?>
	<!-- Jumbotron -->
	<div class="container-fluid" id="divMain">
		<div class="jumbotron">
			<div class="container container-fluid">
				<img src="../images/images.png">
  <p class="alert alert-info">Real and Reel : Buy One get Two Free</p>
  <a class="btn btn-primary btn-lg" href="../index.html" role="button">Back to HomePage</a>
			</div>

</div>
	</div>

	<br>

	<div class="container container-fluid">
   <form action="uploadMovie.php" method="post" enctype="multipart/form-data">
    <!-- comment posting update to requested  -->
  <input type="hidden" name="id" value="<?php echo $id; ?>">

  <div class="row">
    <div class="col">
               <label>Movie Name</label>

      <input type="text" class="form-control" placeholder="Movie Name" name="movie_name" value="<?php echo $movie_name; ?>" required="">
    </div>
    <div class="col">
               <label>Lead Actor</label>

      <input type="text" value="<?php echo $leadactor; ?>" class="form-control" placeholder="Lead Actor" name="lead_actor">
    </div>

  </div>
  <br>
   <div class="row">
    <div class="col">
            <label>Duration</label>

      <input type="number" class="form-control" placeholder="Time in Hours" name="duration" required="">
    </div>
    <div class="col">
      <label>Movie Poster</label>
      <input type="file" name="moviePoster" id="moviePoster" class="form-control"  placeholder="Movie Poster" required="">
    </div>

 
  </div>
  <br>
   <div class="row">
    <div class="col">
            <label>Ratings</label>

      <select class="form-control" name="rate">
        <option value="PG">PG</option>
        <option value="G">G</option>
      </select>
    </div>
    <div class="col">
         <label>Genre</label>

      <select class="form-control"  name="genre">
        <option value="<?php echo $genre; ?>"><?php echo $genre; ?></option>
        <option value="Action">Action</option>
        <option value="Thriller">Thriller</option>
        <option value="drama">Drama</option>
      </select>   
       </div>

  </div>
   
<br>
   <div class="row" style="margin-bottom: 10px;">
        <?php
      if ($update == true):
        ?>
           <div class="col">
      <input type="submit" name="update" class="btn btn-warning btn-block">
    </div>

    <?php
      else:
        ?>
     <div class="col">
      <input type="submit" name="save" value="Submit Movie" class="form-control btn btn-primary btn-block">
    </div>
    <div class="col">
      <input type="reset" class="form-control btn btn-danger btn-block">
    </div>
    <?php endif; ?>
  </div>
</form>


	</div>



<script type="text/javascript" src="../js/style.js"></script>
<script type="text/javascript" src="../bootstrap/js/bootstrap.js"></script>
<script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
</body>
</html>

